require('bootstrap/js/dist/alert');
import $ from 'jquery';
import validate from 'jquery-validation';

import '../../css/front/contact.scss';

$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z ]+$/i.test(value);
});

$('#contact-form').validate({
    rules: {
        "contact[name]": {
            lettersonly: true
        },
        "contact[phone]": {
            digits: true,
            minlength: 9,
            maxlength: 10
        },
        "contact[message]": {
            minlength: 10
        }
    },
    messages: {
        "contact[name]": {
            lettersonly: "Va rugam sa introduceti un nume valid"
        },
        "contact[phone]": {
            digits: "Va rugam sa introduceti doar cifre",
            minlength: "Va rugam sa introduceti un numar de telefon valid",
            maxlength: "Va rugam sa introduceti un numar de telefon valid"
        },
        "contact[message]": {
            minlength: "Va rugam sa introduceti un mesaj din minimum 10 caractere"
        }
    }
});