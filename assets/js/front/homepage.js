import '../../css/front/homepage.scss'

const Homepage = {
    nodes: {
        header: document.querySelector('header')
    },
    init() {
        let self = this;
        window.addEventListener('scroll', () => {
            var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
            if (scrollTop > 0) {
                this.nodes.header.style.background = 'linear-gradient(to right, rgb(117, 46, 176), rgb(30, 11, 67))';
            } else {
                this.nodes.header.removeAttribute('style');
            }
        }, false)
    }
}
Homepage.init();