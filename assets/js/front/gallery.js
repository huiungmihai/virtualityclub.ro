import '../../css/front/gallery.scss'

import Viewer from 'viewerjs';

const gallery = new Viewer(document.getElementById('images'), {
    navbar: 3,
    title: false,
    movable: false,
    filter(image) {
        return image.complete;
    },
    toolbar: {
        zoomIn: 4,
        zoomOut: 4,
        oneToOne: 0,
        reset: 4,
        prev: 4,
        play: {
            show: 0,
            size: 'large',
        },
        next: 4,
        rotateLeft: 4,
        rotateRight: 4,
        flipHorizontal: 4,
        flipVertical: 4,
    },
    url: 'data-src',

});