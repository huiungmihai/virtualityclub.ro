const Hamburger = {
    nodes : {
        targetEl: document.querySelector('.hamburger-menu'),
        sideMenu: document.querySelector('.container-nav'),
        main: document.querySelector('header')
    },
    init() {
        this.nodes.targetEl.addEventListener('click', () => this.toggleMenu(), false);
        window.addEventListener('resize', () => this.resetMenu(), false);
    },
    toggleMenu() {
        this.nodes.main.classList.toggle('menu-active')
    },
    resetMenu() {
        window.outerWidth > 992 ? this.nodes.main.classList.remove('menu-active') : '';
    }
}

Hamburger.init();