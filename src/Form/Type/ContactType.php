<?php

// src/Form/Type/ContactType.php
namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;


class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Nume', 'required' => true])
            ->add('email', EmailType::class, ['label' => 'Email', 'required' => false])
            ->add('phone', TextType::class, ['label' => 'Telefon', 'required' => true])
            ->add('message', TextareaType::class, ['help' => 'In caz de rezervare va rugam sa ne precizati cand doriti programarea.', 'label' => 'Mesaj', 'required' => true, 'attr' => ['rows' => '5']])
        ;
    }

}