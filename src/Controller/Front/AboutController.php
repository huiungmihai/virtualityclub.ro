<?php


namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    #[Route('/ce-reprezinta-realitatea-virtuala',
    name: 'front_about')]
    public function index() {
        //$response = $this->render('front/about.html.twig');
        //$response->setPublic();
        //$response->setMaxAge(3600);
        //$response->headers->addCacheControlDirective('must-revalidate', true);
        //return $response;

        return $this->render('front/about.html.twig');
    }
}