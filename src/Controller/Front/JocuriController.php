<?php


namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class JocuriController extends AbstractController
{
    public $games = [
        'Arizona Sunshine' => 'arizona-sunshine.jpg',
        'Audioshield' => 'audioshield.jpg',
        'Bullet Sorrow' => 'Bullet-Sorrow.jpg',
        'Dead and Buried' => 'Dead-and-Buried.jpg',
        'Dead Effect' => 'Dead-Effect.jpg',
        'Dead Hungry' => 'Dead-Hungry.jpg',
        'Deadly Hunter' => 'Deadly-Hunter.jpg',
        'DiRT Rally' => 'DiRT-Rally.jpg',
        'Echo Arena' => 'Echo-Arena.jpg',
        'Fancy Skiing' => 'Fancy-Skiing.jpg',
        'Fruit Ninja' => 'Fruit-Ninja.jpg',
        'Google Blocks' => 'Google-Blocks.jpg',
        'GORN' => 'GORN.jpg',
        'Gun’s Stories' => 'Gun’s-Stories.jpg',
        'Holodance' => 'Holodance.jpg',
        'Job Simulator' => 'Job-Simulator.jpg',
        'Knockout League' => 'Knockout-League.jpg',
        'Lucky’s Tale' => 'Lucky’s-Tale.jpg',
        'Mech League Box' => 'Mech-League-Box.jpg',
        'No Limits' => 'No-Limits.jpg',
        'Pavlov VR' => 'Pavlov-VR.jpg',
        'PayDay2' => 'PayDay2.jpg',
        'Raw Data' => 'Raw-Data.jpg',
        'Richie’s Plank' => 'Richie’s-Plank.jpg',
        'Rick and Morty' => 'Rick-and-Morty.jpg',
        'Robo Recall' => 'Robo-Recall.jpg',
        'Serious Sam' => 'Serious-Sam.jpg',
        'Sprint Vector' => 'Sprint-Vector.jpg',
        'Stand Out (PUBG)' => 'Stand-Out-PUBG.jpg',
        'SuperHot' => 'SuperHot.jpg',
        'The Climb' => 'The-Climb.jpg',
        'The Lab' => 'The-Lab.jpg',
        'TheBlue' => 'TheBlue.jpg',
        'Tilt Brush' => 'Tilt-Brush.jpg',
        'Multe alte jocuri VR' => 'Advanced-technology-650x400.jpg'

    ];
    #[Route('/jocuri-in-club',
        name: 'front_jocuri')]
    public function index() {
        return $this->render('front/jocuri.html.twig', array('games' => $this->games));
    }
}