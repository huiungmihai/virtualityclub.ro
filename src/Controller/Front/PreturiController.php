<?php


namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PreturiController extends AbstractController
{
    #[Route('/preturi',
    name: 'front_preturi')]
    public function index() {
        return $this->render('front/preturi.html.twig');
    }
}