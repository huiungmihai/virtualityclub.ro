<?php


namespace App\Controller\Front;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use App\Form\Type\ContactType;


class ContactController extends AbstractController
{
    #[Route('/contact',
    name: 'front_contact')]
    public function index(Request $request, MailerInterface $mailer) {

        $form = $this->createForm(ContactType::class, null, ['attr' => ['id' => 'contact-form', 'class' => 'vrs-form']]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $contactFormData = $form->getData();
            $body = 'Nume: ' .  $contactFormData['name'] . '<br>';
            !empty($contactFormData['email']) ? $body .= 'Email: ' . $contactFormData['email'] . '<br>': '' ;
            $body .= 'Telefon: ' . $contactFormData['phone'] . '<br>';
            $body .= 'Mesaj: ' . $contactFormData['message'];

            $email = (new Email())
                ->from('info@virtualityclub.ro')
                ->to('deathllord@yahoo.com')
                //->cc('cc@example.com')
                //->bcc('bcc@example.com')
                //->replyTo('fabien@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject('VirtualityClub')
                ->text('Sending emails is fun again!')
                ->html($body);

            $mailer->send($email);

            $this->addFlash('success', 'Mesajul a fost trimis.');

            return $this->redirectToRoute('front_contact');
        }

        return $this->render('front/contact.html.twig', [
            'contact_form' => $form->createView()
        ]);
    }
}