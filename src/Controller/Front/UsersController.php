<?php

namespace App\Controller\Front;

use App\Entity\Users;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UsersController extends AbstractController
{
    /**
     * @Route("/users/new", name="front_users")
     */
    public function new(EncoderFactoryInterface $encoderFactory) {

        $passwordEncoder = $encoderFactory->getEncoder(Users::class);
        $hashedPassword = $passwordEncoder->encodePassword('123456',null);

        $user = new Users();
        $user->setName('Mihai')
            ->setEmail('mihai@email.com')
            ->setPassword($hashedPassword)
            ->setCreatedAt(new \DateTime('now'));

        dd($user);
    }
}