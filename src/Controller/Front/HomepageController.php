<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    #[Route('/',
    name: 'front_homepage')]
    public function index() {
        return $this->render('front/homepage.html.twig');
    }
}