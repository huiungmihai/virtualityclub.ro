<?php


namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IstoriaVrController extends AbstractController
{
    #[Route('/istoria-vr',
    name: 'front_istoria_vr')]
    public function index() {
        return $this->render('front/istoria_vr.html.twig');
    }
}