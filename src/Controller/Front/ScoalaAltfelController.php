<?php


namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ScoalaAltfelController extends AbstractController
{
    #[Route('/scoala-altfel',
    name: 'front_scoala_altfel')]
    public function index() {
        return $this->render('front/scoala_altfel.html.twig');
    }
}