<?php


namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends AbstractController
{
    #[Route('/galerie',
    name: 'front_gallery')]
    public function index() {
        return $this->render('front/gallery.html.twig');
    }
}