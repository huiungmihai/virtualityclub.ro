<?php


namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;

class MenuBuilder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        // homepage
        $menu
            ->addChild('Home', ['route' => 'front_homepage'])
            ->setAttribute('icon', 'la-home');

        // preturi
        $menu
            ->addChild('Preturi', ['route' => 'front_preturi'])
            ->setAttribute('icon', 'la-hand-holding-usd');

        // galerie
        $menu
            ->addChild('Galerie', ['route' => 'front_gallery'])
            ->setAttribute('icon', 'la-image');

        // jocuri
        $menu
            ->addChild('Jocuri', ['route' => 'front_jocuri'])
            ->setAttribute('icon', 'la-gamepad');

        // istorie
        $menu
            ->addChild('Istoria VR', ['route' => 'front_istoria_vr'])
            ->setAttribute('icon', 'la-history');

        // contact
        $menu
            ->addChild('Contact', ['route' => 'front_contact'])
            ->setAttribute('icon', 'la-address-card');


        // ... add more children

        return $menu;
    }
}
